#include <stdio.h>

struct fraction
{
    int num,den;
};
typedef struct fraction Fraction;

int gcd(int m, int n);


void input(int n, Fraction a[])
{
        for(int j=0;j<n;j++)
    {
        printf("Enter the fraction in A/B for. eg) 23/7 :");
        scanf("%d/%d",&a[j].num,&a[j].den);
    }
}

int gcd(int m,int n)
{
    
   int f;
    
    while (n!=0)
    {
        f=n;
        n=m%n;
        m=f;
    }
        
    return m;
            
}

Fraction reduce(Fraction frac)
{
   int g=gcd(frac.num,frac.den);
   
   frac.num=frac.num/g;
   frac.den=frac.den/g;
   
   return frac;
}


Fraction calculate(int n,Fraction a[])
{
    Fraction frac=a[0];
    
    for(int i=1;i<n;i++)
    {
        frac.num=(frac.num*a[i].den)+(frac.den*a[i].num);
        frac.den=(frac.den*a[i].den);
        
        frac=reduce(frac);
    }
    return frac;
}

void output(int n, Fraction frac, Fraction a[])
{
    for(int i=0;i<n-1;i++)
    {
        printf("%d/%d +",a[i].num,a[i].den);
    }
    
    printf("%d/%d = %d/%d\n",a[n-1].num,a[n-1].den,frac.num,frac.den);
}

int main()
{
    printf("Enter value of n:\n");
    int n;
    scanf("%d",&n);
    
    Fraction a[n],frac;
    
    input(n,a);
    frac=calculate(n,a);
    output(n,frac, a);
    return 0;
}