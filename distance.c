#include <stdio.h>
#include<math.h>
int main()
{
    float x1,y1,x2,y2,d;
    printf ("enter the first co-ordinates \n");
    scanf("%f,%f",&x1,&y1);
    printf ("enter the second co-ordinates \n");
    scanf("%f,%f",&x2,&y2);
    d = sqrt(pow((x2-x1),2)+pow((y2-y1),2));
    printf("the distance between (%.3f,%.3f) and (%.3f,%.3f) is %.4f \n",x1,y1,x2,y2,d);
    return 0;
}