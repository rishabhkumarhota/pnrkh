#include <stdio.h>
#include <math.h>

float input1()
{
    float x;
    printf("enter the x co-ordinates \n");
    scanf("%.3f",&x);
    return x;
}

float input2()
{
    float y;
    printf("enter the y co-ordinates \n");
    scanf("%.3f",&y);
    return y;
}

float cal(int x1, int y1,int x2,int y2)
{
    float d;
    d= sqrt(pow((x2-x1),2)+pow((y2-y1),2));
    return d;
}

    void output(float d)
{
    printf("the distance between the two points is %.4f\n", d);
}
    
int main()
{
     int x1= input1();
     int x2= input1();
     int y1= input2();
     int y2= input2();
     int d= cal(x1,y1,x2,y2);
     output(d);
     return 0;
}


